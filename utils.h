#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>
#include <windows.h>
#include <string.h>

int strInsertChar(char*,char,unsigned char);
int strDeleteChar(char*,unsigned char);

#endif // _UTILS_H_
