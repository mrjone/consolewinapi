#include "gui.h"

int ErrorPrint(HANDLE _hConsole, const char* _text)
{
    SetConsoleTextAttribute(_hConsole,
                            FOREGROUND_RED | FOREGROUND_INTENSITY);
    printf("[ERROR] %s\n", _text);
    SetConsoleTextAttribute(_hConsole,
                            FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN);
    return 0;
}

int KeyProcEvent(KEY_EVENT_RECORD _event, EvenetsGUI* _eGUI)
{
    if (_event.bKeyDown)
    {
        _eGUI->key = _event.wVirtualKeyCode;
        _eGUI->type = REDRAW_PAINT;
        switch(_event.wVirtualKeyCode)
        {
            case VK_ESCAPE:
                return 1;
            case VK_CONTROL:
            case VK_LCONTROL:
            case VK_LSHIFT:
            case VK_LMENU:
            case VK_RMENU:
            case VK_RSHIFT:
            case VK_MENU:
                break;
            case VK_BACK:
                {
                    if (_eGUI->objectsGUI[_eGUI->selectObjectGUI].type == OBJ_TYPE_INPUT_TEXT)
                    {
                        InputText* tmp = _eGUI->objectsGUI[_eGUI->selectObjectGUI].object;
                        CONSOLE_SCREEN_BUFFER_INFO info;
                        GetConsoleScreenBufferInfo(_eGUI->console,
                                               &info);
                        if (strDeleteChar(tmp->text, info.dwCursorPosition.X - tmp->pStart.X))
                        {
                            ErrorPrint(_eGUI->console,
                                       "Cannot remapping memory in strInsertChar()");
                            exit(-1);
                        }
                        SetEvent(_eGUI->eventRedraw);
                    }
                    break;
                }
            default:
                if (_eGUI->objectsGUI[_eGUI->selectObjectGUI].type == OBJ_TYPE_INPUT_TEXT)
                {
                    InputText* _tmp = _eGUI->objectsGUI[_eGUI->selectObjectGUI].object;
                    CONSOLE_SCREEN_BUFFER_INFO info;
                    GetConsoleScreenBufferInfo(_eGUI->console,
                                               &info);
                    if (strInsertChar(_tmp->text,
                                      _eGUI->key,
                                      info.dwCursorPosition.X - _tmp->pStart.X))
                    {
                        ErrorPrint(_eGUI->console,
                                   "Cannot remapping memory in strInsertChar()");
                        exit(-1);
                    }
                    SetEvent(_eGUI->eventRedraw);
                }
                break;
        }
    }
    return 0;
}

DWORD WINAPI tGuiRedraw(LPVOID _lpParam)
{
    DWORD eventResult;
    EvenetsGUI* event = _lpParam;
    while(1)
    {
        eventResult = WaitForSingleObject(event->eventRedraw,
                                          INFINITE);
        switch(eventResult)
        {
        case WAIT_OBJECT_0:
        {
            switch(event->type)
            {
                case REDRAW_RESIZE:
                {
                    break;
                }
                case REDRAW_PAINT:
                {
                    COORD statPoint, endPoint;
                    statPoint.X = 0;
                    statPoint.Y = 0;
                    DWORD lenFill;
                    CONSOLE_SCREEN_BUFFER_INFO infoConsole;
                    GetConsoleScreenBufferInfo(event->console,
                                               &infoConsole);
                    FillConsoleOutputAttribute(event->console,
                                               BACKGROUND_BLUE | BACKGROUND_INTENSITY,
                                               infoConsole.dwSize.X * infoConsole.dwSize.Y,
                                               statPoint,
                                               &lenFill);
                    //-----------

                    if (event->countObjectsGUI > 0)
                    for (unsigned char i = 0; i < event->countObjectsGUI; i++)
                    {
                        switch(event->objectsGUI[i].type)
                        {
                        case OBJ_TYPE_INPUT_TEXT:
                            {
                                InputText* _text = event->objectsGUI[i].object;
                                DrawInputText(*_text,
                                              event->console);
                                break;
                            }
                        case OBJ_TYPE_BUTTON:
                            {
                                break;
                            }
                        case OBJ_TYPE_LABLE:
                            {
                                break;
                            }
                        default:
                            {
                                ErrorPrint(event->console,
                                           "NOT DEFINED OBJECT");
                                printf("%d", event->objectsGUI[i].type);
                                exit(-1);
                            }
                        }
                    }

                    break;
                }
                default:
                {
                    break;
                }
            }
            break;
        }
        default:
            break;
        }
        ResetEvent(event->eventRedraw);
    }
    return 0;
}

int DrawRectangle(COORD _pStart, COORD _pEnd, HANDLE _console, WORD _attr)
{
    DWORD lenDraw;
    for (unsigned char i = _pStart.Y; i < _pEnd.Y; i++,_pStart.Y++)
    {
        FillConsoleOutputAttribute(_console,
                                   _attr,
                                   _pEnd.X - _pStart.X - 1,
                                   _pStart,
                                   &lenDraw);
    }
    return lenDraw;
}

int DrawInputText(InputText _entry, HANDLE _console)
{
    COORD endPoint;
    endPoint.X = _entry.pStart.X + _entry.sizeText.X;
    endPoint.Y = _entry.pStart.Y + _entry.sizeText.Y;
    DrawRectangle(_entry.pStart,
                  endPoint,
                  _console,
                  _entry.attributes);
    SetConsoleCursorPosition(_console,  _entry.pStart);

    SetConsoleTextAttribute(_console,
                            _entry.attributes);
    for (unsigned char i = 0; i < _entry.lenText; i++)
        if (_entry.type == INPUT_TYPE_PASS)
            putchar('*');
        else
            putchar(_entry.text[i]);

    return 0;
}

int AddObjectToEvent(EvenetsGUI* _evenet, Object _obj)
{
    Object* _tmp = (Object*)malloc((_evenet->countObjectsGUI+1)*sizeof(Object));
    if (_tmp == NULL)
        return 1;
    if (_evenet->countObjectsGUI > 0)
    {
        for (unsigned char i = 0; i < _evenet->countObjectsGUI; i++)
        {
            _tmp[i].object = _evenet->objectsGUI[i].object;
            _tmp[i].type = _evenet->objectsGUI[i].type;
            printf("TMP: %d : %d\n", _tmp[i].object, _tmp[i].type);
        }
        free(_evenet->objectsGUI);
    }
    _evenet->objectsGUI = (Object*)malloc(_evenet->countObjectsGUI*sizeof(Object));
    if (_evenet->objectsGUI == NULL)
        return 1;
    for (unsigned char i = 0; i < _evenet->countObjectsGUI; i++)
    {
        _evenet->objectsGUI[i].object = _tmp[i].object;
        _evenet->objectsGUI[i].type = _tmp[i].type;
        printf("EVENT: %d : %d\n", _evenet->objectsGUI[i].object, _evenet->objectsGUI[i].type);
    }
    _evenet->countObjectsGUI++;
    _evenet->objectsGUI[_evenet->countObjectsGUI-1].object = _obj.object;
    _evenet->objectsGUI[_evenet->countObjectsGUI-1].type = _obj.type;
    free(_tmp);

    return 0;
}

int ITinit(InputText* _it, const char* _text, unsigned char _type)
{
    _it->type = _type;
    _it->pStart.X = 0;
    _it->pStart.Y = 0;
    _it->lenText = strlen(_text);
    _it->text = (char*)malloc(_it->lenText*sizeof(char));
    if (_it->text == NULL)
        return 1;
    strcpy(_it->text, _text);
    return 0;
}

int ITsetPosition(InputText* _it, int _x, int _y)
{
    _it->pStart.X = _x;
    _it->pStart.Y = _y;
    return 0;
}

int ITsetSize(InputText* _it, int _w, int _h)
{
    _it->sizeText.X = _w;
    _it->sizeText.Y = _h;
    return 0;
}

int ITsetText(InputText* _it, const char* _text)
{
    free(_it->text);
    _it->lenText = strlen(_text);
    _it->text = (char*)malloc(_it->lenText*sizeof(char));
    strcpy(_it->text, _text);
    return 0;
}
