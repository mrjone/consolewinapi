#include <stdio.h>
#include <windows.h>
#include "gui.h"
#include "utils.h"

#define DEFAULT_TITLE "Window is so big..."

int main()
{
    HANDLE hConsoleOut,
        hConsoleIn,
        hThreadGUI;

    EvenetsGUI events;
    events.countObjectsGUI = 0;


    INPUT_RECORD irBuffer[128];
    DWORD countEvent,
        threadID;

    hConsoleIn = GetStdHandle(STD_INPUT_HANDLE);
    if (hConsoleIn == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] STDIN cannot be get!\n");
        exit(1);
    }
    hConsoleOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if (hConsoleOut == INVALID_HANDLE_VALUE)
    {
        printf("[ERROR] STDOUT cannot be get!\n");
        exit(2);
    }

    if (!SetConsoleTitle(DEFAULT_TITLE))
    {
        ErrorPrint(hConsoleOut, "Cannot set console title!");
    }

    if (!SetConsoleMode(hConsoleIn,
                        ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT))
    {
        ErrorPrint(hConsoleOut, "Cannot set console mode!");
        exit(GetLastError());
    }

    Object tmp;
    InputText passEntry;
    passEntry.attributes = COLOR_TEXT_WHITE;
    if (ITinit(&passEntry,
               "Test",
               INPUT_TYPE_PASS))
    {
        ErrorPrint(hConsoleOut,
                   "Cannot init INPUT_TEXT");
        exit(-1);
    }

    ITsetSize(&passEntry,
              20,
              1);
    ITsetPosition(&passEntry,
                  20,10);

    tmp.object = &passEntry;
    tmp.type = OBJ_TYPE_INPUT_TEXT;

    if (AddObjectToEvent(&events, tmp))
    {
        ErrorPrint(hConsoleOut,
                   "Cricital! Cannot created objects!");
        exit(-1);
    }

    events.eventRedraw = CreateEvent(NULL,
                                     TRUE,
                                     FALSE,
                                     TEXT("eventRedraw"));
    events.console = hConsoleOut;

    hThreadGUI = CreateThread(NULL,
                              0,
                              (LPTHREAD_START_ROUTINE)tGuiRedraw,
                              &events,
                              0,
                              &threadID);
    if (hThreadGUI == NULL)
    {
        ErrorPrint(hConsoleOut, "Thread creating failed!");
        exit(GetLastError());
    }

    events.type = REDRAW_PAINT;
    events.key = 0;
    SetEvent(events.eventRedraw);


    while(1)
    {
        if (!ReadConsoleInput(hConsoleIn,
                              irBuffer,
                              128,
                              &countEvent))
        {
            ErrorPrint(hConsoleOut, "Read input event failed!");
            exit(GetLastError());
        }

        for (unsigned char i = 0; i < countEvent; i++)
        {
            switch(irBuffer[i].EventType)
            {
                case KEY_EVENT:
                    if (KeyProcEvent(irBuffer[i].Event.KeyEvent, &events))
                    {
                        printf("Program close!\n");
                        CloseHandle(events.eventRedraw);
                        CloseHandle(hThreadGUI);
                        exit(0);
                    }
                    break;
                case MOUSE_EVENT:
                    break;
                case WINDOW_BUFFER_SIZE_EVENT:
                    break;
                default:
                    break;
            }
        }
    }

    return 0;
}

