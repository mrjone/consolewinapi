#include "utils.h"

int strInsertChar(char* _str,char _c,unsigned char _n)
{
    unsigned char lenStr = strlen(_str);
    lenStr++;
    char* tmp = (char*)malloc(lenStr*sizeof(char));
    if (tmp == NULL)
        return 1;

    for (unsigned char i = 0; i < lenStr; i++)
    {
        if (i < _n)
            tmp[i] = _str[i];
        else
        {
            if (i == _n)
                tmp[i] = _c;
            else
                tmp[i] = _str[i-1];
        }
    }
    free(_str);
    _str = (char*)malloc(lenStr*sizeof(char));
    if (_str == NULL)
        return 1;

    strcpy(_str, tmp);
    free(tmp);

    return 0;
}

int strDeleteChar(char* _str,unsigned char _n)
{
    unsigned char lenStr = strlen(_str);
    lenStr--;
    char* tmp = (char*)malloc(lenStr*sizeof(char));
    if (tmp == NULL)
        return 1;

    for (unsigned char i = 0; i < lenStr+1; i++)
    {
        if (i < _n)
            tmp[i] = _str[i];
        else
            if (i > _n)
                tmp[i-1] = _str[i];
    }

    free(_str);
    _str = (char*)malloc(lenStr*sizeof(char));
    if (_str == NULL)
        return 1;
    strcpy(_str, tmp);
    free(tmp);

    return 0;
}
