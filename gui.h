#ifndef _GUI_H_
#define _GUI_H_

#include <stdio.h>
#include <windows.h>
#include <string.h>
#include "utils.h"

#define COLOR_TEXT_RED          \
        FOREGROUND_RED
#define COLOR_TEXT_LIGHT_RED    \
        FOREGROUND_RED | FOREGROUND_INTENSITY
#define COLOR_TEXT_BLUE         \
        FOREGROUND_BLUE
#define COLOR_TEXT_LIGHT_BLUE   \
        FOREGROUND_BLUE | FOREGROUND_INTENSITY
#define COLOR_TEXT_GREEN        \
        FOREGROUND_GREEN
#define COLOR_TEXT_LIGHT_GREEN  \
        FOREGROUND_GREEN | FOREGROUND_INTENSITY
#define COLOR_TEXT_BLACK        \
        0
#define COLOR_TEXT_PURPURE      \
        FOREGROUND_RED | FOREGROUND_BLUE
#define COLOR_TEXT_PINK         \
        FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_INTENSITY
#define COLOR_TEXT_YELLOW       \
        FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY
#define COLOR_TEXT_ORANGE       \
        FOREGROUND_GREEN | FOREGROUND_BLUE
#define COLOR_TEXT_GRAY         \
        FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED
#define COLOR_TEXT_WHITE        \
        FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY


#define OBJ_TYPE_INPUT_TEXT     0
#define OBJ_TYPE_BUTTON         1
#define OBJ_TYPE_LABLE          2

typedef struct t_Object
{
    PVOID *object;
    WORD type;
} Object;

#define INPUT_TYPE_PASS 0
#define INPUT_TYPE_TEXT 1

typedef struct t_InputText
{
    char* text;
    unsigned char lenText;
    unsigned char type;
    WORD attributes;
    COORD pStart;
    COORD sizeText;
} InputText;

#define REDRAW_RESIZE   0
#define REDRAW_PAINT    1
#define REDRAW_OFF      2

typedef struct t_Evenets
{
    HANDLE eventRedraw;
    HANDLE console;
    unsigned char type;
    DWORD key;
    DWORD keyMouse;
    COORD sizeWin;
    Object* objectsGUI;
    unsigned char countObjectsGUI;
    unsigned char selectObjectGUI;
} EvenetsGUI;

int ErrorPrint(HANDLE, const char*);
int KeyProcEvent(KEY_EVENT_RECORD, EvenetsGUI*);
int DrawRectangle(COORD, COORD, HANDLE, WORD);
int DrawInputText(InputText, HANDLE);

int AddObjectToEvent(EvenetsGUI*, Object);

int ITinit(InputText*, const char*, unsigned char);
int ITsetPosition(InputText*, int, int);
int ITsetSize(InputText*, int, int);
int ITsetText(InputText*, const char*);

DWORD WINAPI tGuiRedraw(LPVOID);

#endif // _GUI_H_
